import React from "react";

function Avatar({user}) {
    return user ? (<img src={user.photo} />) : null;
}
export default Avatar;
