import React from "react";
import {Link} from "react-router-dom";

function HomePage() {

    return (
        <div className="card">
            <h1>Home page</h1>
            <Link to={"/users"}>User list</Link>
            <br />
            <Link to={"/users/new"}>Add user</Link>
        </div>
    )
}

export default HomePage;
