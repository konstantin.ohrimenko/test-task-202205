import React, {useEffect, useRef, useState} from "react";
import './styles.css';
import {useDispatch, useSelector} from "react-redux";
import {loadPositionList} from "../../redux/position/actions";
import {getPositionList} from "../../redux/position/selectors";
import {requestToken} from "../../redux/auth/actions";
import {createUser} from "../../redux/user/actions";

function TextError({error}) {
    if (!error) {
        return null;
    }
    return <span className={"error"}>{error}</span>;
}

function Error({name, errors}) {
    const fails = errors?.fails || {};
    const error = fails[name] && fails[name][0];
    return <TextError error={error} />
}

function Group({label, children, ...rest}) {
    return (
        <div className={"text-input-group"}>
            <label>{label}</label>
            {children}
            <Error {...rest} />
        </div>
    );
}

function TextField({value, onChange, ...rest}) {
    const onChangeHandler = (e) => {
        onChange(e.target.value);
    };
    return (
        <Group {...rest}>
            <input name={rest.name} type={"text"} value={value} onChange={onChangeHandler} />
        </Group>
    );
}

function FileField({value, onChange, ...rest}) {
    const onChangeHandler = (e) => {
        onChange(e.target.value);
    };
    return (
        <Group {...rest}>
            <input type={"file"} name={rest.name} value={value} onChange={onChangeHandler} />
        </Group>
    );
}

function SelectField({value, onChange, items, ...rest}) {
    const onChangeHandler = (e) => {
        onChange(e.target.value);
    };
    const options = items.map(item => {
        const attrs = {
            key: item.id,
            value: item.id,
        };
        if (item.id === value) {
            attrs['selected'] = 'selected';
        }
        return <option {...attrs}>{item.name}</option>;
    });
    return (
        <Group {...rest}>
            <select name={rest.name} onChange={onChangeHandler}>
                <option value="">--select position--</option>
                {options}
            </select>
        </Group>
    );
}

function UserCreatePage() {
    const [errors, setErrors] = useState();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [positionId, setPositionId] = useState(0);
    const [photo, setPhoto] = useState('');
    const [isPending, setIsPending] = useState(false);

    const positions = useSelector(getPositionList);

    const formRef = useRef();

    const dispatcher = useDispatch();
    useEffect(function() {
        dispatcher(loadPositionList())
    }, []);

    const sendForm = token => {
        const body = new FormData(formRef.current)
        const payload = {
            body,
            headers: {
                token,
            },
            success: response => {
                const { data } = response;
                alert(`${data.message} with Id ${data.user_id}`);
                setName('');
                setEmail('');
                setPhone('');
                setPositionId(0);
                setPhoto('');
                setIsPending(false);
            },
            error: error => {
                setErrors(error?.response?.data);
                setIsPending(false);
            }
        };
        dispatcher(createUser(payload));
    };

    const requestNewToken = () => {
        const payload = {
            success: response => {
                const { data: { token } } = response;
                sendForm(token);
            },
            error: error => {
                setErrors(error?.response?.data);
                setIsPending(false);
            }
        };
        dispatcher(requestToken(payload))
    }

    const submit = () => {
        if (isPending) {
            return;
        }
        setIsPending(true);
        setErrors(null);
        requestNewToken();
    }

    return (
        <div className={"card"}>
            <h1>Create new user</h1>

            <TextError error={errors?.message} />

            <form ref={formRef}>
                <TextField onChange={setName} value={name} name={"name"} label={"Username"} errors={errors} />
                <TextField onChange={setEmail} value={email} name={"email"} label={"E-Mail"} errors={errors} />
                <TextField onChange={setPhone} value={phone} name={"phone"} label={"Phone number"} errors={errors} />
                <SelectField onChange={setPositionId} items={positions} value={positionId} name={"position_id"} label={"Position"} errors={errors} />
                <FileField onChange={setPhoto} value={photo} name={"photo"} label={"Photo"} errors={errors} />
            </form>

            <button type="button" onClick={submit}>{isPending ? 'Creating' : 'Submit'}</button>

        </div>
    );
}
export default UserCreatePage;
