import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentPage, getUserList, isPending, nextPage} from "../../redux/user/selectors";
import {loadUserList} from "../../redux/user/actions";
import './styles.css';
import {Link} from "react-router-dom";
import Avatar from "../../Components/Avatar";

function LinkTd({userId, children}) {
    return (
        <td>
            <Link to={`/users/${userId}`}>
                {children}
            </Link>
        </td>
    );
}

function Row({user}) {
    return (
        <tr>
           <td>
               <Avatar user={user} />
           </td>
           <LinkTd userId={user.id}>{user.id}</LinkTd>
           <LinkTd userId={user.id}>{user.name}</LinkTd>
           <LinkTd userId={user.id}>{user.email}</LinkTd>
           <LinkTd userId={user.id}>{user.phone}</LinkTd>
           <LinkTd userId={user.id}>{user.position}</LinkTd>
           <td>{user.registration_timestamp}</td>
        </tr>
    );
}

function UserListPage() {
    const users = useSelector(getUserList);
    const next = useSelector(nextPage);
    const page = useSelector(currentPage);
    const pending = useSelector(isPending);

    const dispatcher = useDispatch();

    const loadPage = pageIndex => {
        dispatcher(loadUserList({page: pageIndex}));
    };

    const onLoadMoreClick = () => {
        if (pending) {
            return;
        }
        loadPage(next);
    };

    useEffect(() => loadPage(1), []);

    const text = pending ? 'Loading...' : 'Load more';
    const rows = users.map(user => <Row key={user.id} user={user} />)
    return (
        <div className={"card"}>
            <h1>User list</h1>

            <table>
                <thead>
                <tr>
                    <th>Photo</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>E-Mail</th>
                    <th>Phone</th>
                    <th>Position</th>
                    <th>Registration timestamp</th>
                </tr>
                </thead>
                <tbody>{rows}</tbody>
                <tfoot>
                <tr>
                    <td colSpan={6}>{`Current page: ${page}`}</td>
                    <td style={{textAlign: 'right'}}>
                        {next ? <button onClick={onLoadMoreClick} type="button">{text}</button> : null}
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    )
}

export default UserListPage;
