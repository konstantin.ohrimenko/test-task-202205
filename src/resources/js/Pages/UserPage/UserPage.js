import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {useDispatch} from "react-redux";
import {loadUser} from "../../redux/user/actions";
import Avatar from "../../Components/Avatar";

function UserPage() {
    const { userId } = useParams();
    const dispatcher = useDispatch();
    const [currentUser, setCurrentUser] = useState(null);

    useEffect(function() {
        dispatcher(loadUser({
            id: userId,
            success: function(result) {
                const { data: { user } } = result;
                setCurrentUser(user);
            }
        }));
    }, []);

    const keys = Object.keys(currentUser || {});
    const rows = keys.map(function(key) {
        return (
            <li key={key}><b>{key}:</b> {currentUser[key]}</li>
        )
    });

    const title = currentUser ? `: ${currentUser.name}` : ' - Loading';
    return (
        <div className={"card"}>
            <h1>User info{title}</h1>
            <div>
                <table>
                    <tbody>
                    <tr>
                        <td style={{textAlign: 'center'}}>
                            <Avatar user={currentUser} />
                        </td>
                        <td>
                            <ul>
                                {rows}
                            </ul>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default UserPage;
