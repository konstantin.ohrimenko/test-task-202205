import {takeLatest} from 'redux-saga/effects';
import makeRequest from '../../store/makeRequest';
import api from '../../store/api';
import {READ} from '../core';
import {POSITIONS} from './types';
import callback from "../../store/callback";

function* loadPositionListWorker(action) {
  const result = yield makeRequest(api.positions.loadList, action.payload)({
    type: action.type,
    payload: action.payload,
  });
  callback(result, action.payload)
}

export function* positionDataWatcher() {
  yield takeLatest(POSITIONS + READ, loadPositionListWorker);
}
