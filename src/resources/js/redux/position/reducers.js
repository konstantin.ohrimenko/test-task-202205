import {PENDING, SUCCESS, ERROR, READ} from '../core';
import {POSITIONS} from './types';

const initialState = {
  positions: [],
  pending: false,
  error: false,
};

export default function positionReducer(state = initialState, action) {
  switch (action.type) {
    // USER LIST
    case POSITIONS + READ + PENDING:
      return {
        ...state,
        pending: true,
        error: false,
      };
    case POSITIONS + READ + SUCCESS:
      let { result } = action.payload;
      return {
        ...state,
        positions: result.positions,
        pending: false,
        error: false,
      };
    case POSITIONS + READ + ERROR:
      return {
        ...state,
        pending: false,
        error: true,
      };
    default:
      return state;
  }
}
