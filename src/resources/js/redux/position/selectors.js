export const isPending = state => !!state.position.pending;
export const getPositionList = state => state.position.positions;
