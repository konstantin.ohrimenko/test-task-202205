import {READ} from '../core';
import {POSITIONS} from './types';

export function loadPositionList(payload) {
  return {
    type: POSITIONS + READ,
    payload,
  };
}
