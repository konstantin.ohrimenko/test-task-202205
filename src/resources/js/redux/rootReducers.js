import {combineReducers} from 'redux';
import auth from './auth/reducers';
import user from './user/reducers';
import position from './position/reducers';

const rootReducer = combineReducers({
  auth,
  user,
  position,
});

// export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
