import {all} from 'redux-saga/effects';
import es6promise from 'es6-promise';
import 'isomorphic-unfetch';
import {authDataWatcher} from './auth/sagas';
import {userDataWatcher} from './user/sagas';
import {positionDataWatcher} from "./position/sagas";

es6promise.polyfill();

function* rootSaga() {
  yield all([
    authDataWatcher(),
    userDataWatcher(),
    positionDataWatcher(),
  ]);
}

export default rootSaga;
