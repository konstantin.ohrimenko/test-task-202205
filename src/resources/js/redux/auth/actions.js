import {READ} from '../core';
import {TOKEN} from './types';

export function requestToken(payload) {
  return {
    type: TOKEN + READ,
    payload,
  };
}
