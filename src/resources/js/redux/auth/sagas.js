import {takeLatest} from 'redux-saga/effects';
import makeRequest from '../../store/makeRequest';
import api from '../../store/api';
import {READ} from '../core';
import {TOKEN} from './types';
import callback from "../../store/callback";

function* requestTokenWorker(action) {
  const result = yield makeRequest(api.auth.token)({
    type: action.type,
    payload: action.payload,
  });
  callback(result, action.payload)
}

export function* authDataWatcher() {
  yield takeLatest(TOKEN + READ, requestTokenWorker);
}
