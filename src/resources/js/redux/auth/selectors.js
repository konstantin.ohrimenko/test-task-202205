export const isPending = state => !!state.auth.pending;
export const getToken = state => state.auth?.token;
