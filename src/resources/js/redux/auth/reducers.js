import {PENDING, SUCCESS, ERROR, READ} from '../core';
import {TOKEN} from './types';

const initialState = {
  token: '',
  pending: false,
  error: false,
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    // GET USER PROFILE
    case TOKEN + READ + PENDING:
      return {
        ...state,
        pending: true,
        error: false,
      };
    case TOKEN + READ + SUCCESS:
      return {
        ...state,
        pending: false,
        error: false,
        token: action.payload.result,
      };
    case TOKEN + READ + ERROR:
      return {
        ...state,
        pending: false,
        error: true,
      };
    default:
      return state;
  }
}
