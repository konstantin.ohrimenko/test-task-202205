export const isPending = state => !!state.user.pending;
export const currentPage = state => state.user.page;
export const nextPage = state => state.user.nextPage;
export const getUserList = state => state.user.users;
