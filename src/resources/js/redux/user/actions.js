import {CREATE, ONE, READ} from '../core';
import {USERS} from './types';

export function loadUserList(payload) {
  return {
    type: USERS + READ,
    payload,
  };
}

export function loadUser(payload) {
  return {
    type: USERS + ONE + READ,
    payload,
  };
}

export function createUser(payload) {
  return {
    type: USERS + CREATE,
    payload,
  };
}
