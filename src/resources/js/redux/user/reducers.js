import {PENDING, SUCCESS, ERROR, READ} from '../core';
import {USERS} from './types';

const initialState = {
  users: [],
  page: 0,
  nextPage: 1,
  pending: false,
  error: false,
};

function mergeUsers(page, users, data) {
    if (page === 1) {
        return data;
    }
    return [
        ...users,
        ...data,
    ];
}

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    // USER LIST
    case USERS + READ + PENDING:
      return {
        ...state,
        pending: true,
        error: false,
      };
    case USERS + READ + SUCCESS:
      let { result, request: { page } } = action.payload;
      return {
        ...state,
        users: mergeUsers(page, state.users, result.users),
        page: result.page,
        nextPage: result?.links?.next_url ? result.page + 1 : null,
        pending: false,
        error: false,
      };
    case USERS + READ + ERROR:
      return {
        ...state,
        pending: false,
        error: true,
      };
    default:
      return state;
  }
}
