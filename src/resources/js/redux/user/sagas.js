import {takeLatest} from 'redux-saga/effects';
import makeRequest from '../../store/makeRequest';
import api from '../../store/api';
import {CREATE, ONE, READ} from '../core';
import {USERS} from './types';
import callback from "../../store/callback";

function* loadUserListWorker(action) {
  const result = yield makeRequest(api.users.loadList, action.payload)({
    type: action.type,
    payload: action.payload,
  });
  callback(result, action.payload)
}

function* loadUserWorker(action) {
  const result = yield makeRequest(api.users.loadUser, action.payload)({
    type: action.type,
    payload: action.payload,
  });
  callback(result, action.payload)
}

function* createUserWorker(action) {
  const result = yield makeRequest(api.users.createUser, action.payload)({
    type: action.type,
    payload: action.payload,
  });
  callback(result, action.payload)
}

export function* userDataWatcher() {
  yield takeLatest(USERS + READ, loadUserListWorker);
  yield takeLatest(USERS + ONE + READ, loadUserWorker);
  yield takeLatest(USERS + CREATE, createUserWorker);
}
