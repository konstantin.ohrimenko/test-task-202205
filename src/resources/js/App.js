import * as React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import UserListPage from "./Pages/UserListPage/UserListPage";
import HomePage from "./Pages/HomePage/HomePage";
import UserPage from "./Pages/UserPage/UserPage";
import UserCreatePage from "./Pages/UserCreatePage/UserCreatePage";

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path='users' element={<UserListPage />} />
                <Route path='users/:userId' element={<UserPage />} />
                <Route path='users/new' element={<UserCreatePage />} />
            </Routes>
        </BrowserRouter>
    );
}

export default App;
