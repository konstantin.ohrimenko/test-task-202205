import rootReducers from '../redux/rootReducers';
import {applyMiddleware, createStore} from 'redux';
import axios from 'axios';
import createSagaMiddleware from 'redux-saga';
import rootSagas from '../redux/rootSagas';

const API_HOST = `//${location.host}`;

const errorResponseCallback = error => {
  const {
    response: {data, status, statusText},
  } = error;
  if (status) {
    let msg = '';
    try {
      msg = ': ' + data.error.message;
    } catch (e) {}
    if (status === 400) {
      console.log('Bad request \n' + msg);
    } else if (status === 401) {
      console.log('Not auth \n' + msg);
    } else if (status === 403) {
      console.log('Not enough rights \n' + msg);
    } else if (status === 500) {
      console.log(status + '\n Server error \n' + msg);
    } else {
      console.log(status + '/' + statusText + '/' + msg);
    }
  }

  return Promise.reject(error);
};

function getMiddleware() {
  const api = axios.create({
    baseURL: API_HOST,
  });
  api.interceptors.response.use(response => response, errorResponseCallback);
  return createSagaMiddleware({
    context: {api},
  });
}

const sagaMiddle = getMiddleware();

const bindMiddleware = middleware => {
  if (process.env.NODE_ENV === 'development') {
    applyMiddleware(...middleware);
  }
  return applyMiddleware(...middleware);
};

export const store = createStore(
  rootReducers,
  {},
  bindMiddleware([sagaMiddle]),
);

export const initSagas = () => {
  store.runSaga = () => {
    if (store.saga) {
      return;
    }
    store.saga = sagaMiddle.run(rootSagas);
    if (store.saga) {
      return store.saga;
    } else {
      store.saga = sagaMiddle.run(rootSagas);
    }
  };

  sagaMiddle.run(rootSagas);
};
