import axios from 'axios';

const API_HOST = `//${location.host}`;

export const makeApi = token => {
  return axios.create({
    baseURL: API_HOST,
    headers: {
      common: {
        Authorization: token ? 'Bearer ' + token : '',
      },
    },
  });
};

export const defaultApi = makeApi();

const apis = {
  auth: {
    token: ({api = defaultApi} = {}) => api.get('/api/v1/token'),
  },
  users: {
      loadList: ({api = defaultApi, data} = {}) => api.get(`/api/v1/users?page=${data.page}`),
      loadUser: ({api = defaultApi, data} = {}) => api.get(`/api/v1/users/${data.id}`),
      createUser: ({api = defaultApi, data} = {}) => api.post(`/api/v1/users`, data.body, {headers: data.headers}),
  },
  positions: {
      loadList: ({api = defaultApi} = {}) => api.get(`/api/v1/positions`),
  },
};

export default apis;
