import {put, call, getContext} from 'redux-saga/effects';

export default function makeRequest(
  getData,
  data = undefined,
  settings = null,
) {
  return function* (action) {
    // const isDev = process.env.NODE_ENV === 'development';
    const isDev = false;
    const api = yield getContext('api');
    if (isDev) {
      // console.time(action.type);
    }
    const {...rest} = action.payload || {};
    yield put({
      type: `${action.type}/PENDING`,
      payload: {
        ...rest,
        data,
      },
    });
    try {
      const response = yield call(getData, {
        api,
        ...(data ? {data} : rest || {}),
        ...(settings && settings),
      });
      if (isDev) {
        console.timeEnd(action.type);
      }
      yield put({
        type: `${action.type}/SUCCESS`,
        payload: {result: response.data, request: {...rest}},
      });
      return {result: response};
    } catch (error) {
      if (isDev) {
        console.timeEnd(action.type);
        console.error('request error', error || typeof error);
      }
      yield put({
        type: `${action.type}/ERROR`,
        payload: {error, request: {...rest}},
      });
      return {error};
    }
  };
}
