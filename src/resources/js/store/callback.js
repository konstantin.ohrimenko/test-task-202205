function callback({result, error}, payload) {
    if (result && typeof payload?.success === 'function') {
        payload.success(result);
    }
    else if (error && typeof payload?.error === 'function') {
        payload.error(error);
    }
}

export default callback;
