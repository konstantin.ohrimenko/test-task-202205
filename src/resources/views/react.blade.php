<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Demo application</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <style>
        body, html {
            font-family: 'Nunito', sans-serif;
            padding: 0;
            margin: 0;
        }
    </style>
</head>
<body>
    <div id="root"></div>
    <script src="/js/index.js"></script>

</body>
</html>
