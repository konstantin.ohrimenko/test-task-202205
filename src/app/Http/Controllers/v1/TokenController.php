<?php

namespace App\Http\Controllers\v1;


use App\Http\Controllers\Controller;
use Carbon\CarbonImmutable;
use Illuminate\Support\Str;
use Lcobucci\JWT\Configuration;

class TokenController extends Controller
{
    public function __invoke(Configuration $config)
    {
        $now = CarbonImmutable::now();
        $token = $config->builder()
            // Token unique key
            ->identifiedBy(Str::uuid())
            // Configures the issuer (iss claim)
            ->issuedBy(config('app.url'))
            // Configures the time that the token was issue (iat claim)
            ->issuedAt($now)
            ->canOnlyBeUsedAfter($now)
            // Configures the expiration time of the token (exp claim)
            ->expiresAt($now->addMinutes(config('jwt.ttl')))
            // Builds a new token
            ->getToken($config->signer(), $config->signingKey());
        return response()->json([
            'success' => true,
            'token' => $token->toString(),
        ]);
    }

}
