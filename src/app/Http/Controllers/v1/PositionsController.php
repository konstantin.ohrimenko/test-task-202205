<?php

namespace App\Http\Controllers\v1;


use App\Http\Controllers\Controller;
use App\Http\Resources\PositionCollection;
use App\Models\Position;

class PositionsController extends Controller
{
    public function __invoke()
    {
        $positions = Position::all();
        return new PositionCollection($positions);
//        return PositionCollection::collection($positions);
    }
}
