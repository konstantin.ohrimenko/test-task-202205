<?php

namespace App\Http\Controllers\v1;

use App\Exceptions\CustomValidationException;
use App\Exceptions\NotFound;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserViewRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Jobs\TinifyImage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $count = $request->get('count', 6);
        $users = User::query()->orderBy('id')->paginate($count);
        return new UserCollection($users);
    }

    public function view(UserViewRequest $request, $userId)
    {

        $user = User::find($userId);
        if (!$user) {
            throw new NotFound(
                'The user with the requested identifier does not exist',
                ['user_id' => 'User not found']
            );
        }

        return new UserResource($user);
    }

    public function create(UserCreateRequest $request)
    {
        $email = $request->input('email');
        $phone = $request->input('phone');
        if (User::query()->where('email', $email)->orWhere('phone', $phone)->count()) {
            throw new CustomValidationException(
                'User with this phone or email already exist',
                null,
                409
            );
        }
        $data = $request->validated();
        $user = User::create($data);

        $photo = $request->file('photo');
        $user->photo = $this->uploadImage($user->id, $photo);
        $user->save();

        TinifyImage::dispatch($user);

        return [
            "success" => true,
            "user_id" => $user->id,
            "message" => "New user successfully registered",
        ];
    }

    /**
     * @param UploadedFile $photo
     */
    private function uploadImage($id, $photo)
    {
        $filename = "{$id}.jpg";
        $path = 'avatar';
        $filepath = "{$path}/{$filename}";

        $resize = \Image::make($photo->path())->fit(70)->encode('jpg', 100);
        Storage::disk('public')->put($filepath, $resize);
        return $filepath;
    }
}
