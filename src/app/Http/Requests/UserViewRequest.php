<?php

namespace App\Http\Requests;

class UserViewRequest extends CustomFormRequest
{
    protected $statusCode = 400;

    protected function prepareForValidation()
    {
        $this->merge($this->route()->parameters());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'integer',
        ];
    }

    public function messages()
    {
        return [
            'integer' => 'The user_id must be an integer.',
        ];
    }

}
