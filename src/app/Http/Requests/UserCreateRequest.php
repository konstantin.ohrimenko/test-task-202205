<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;

class UserCreateRequest extends CustomFormRequest
{
    public const IMAGE_MIN_HEIGHT = 70;
    public const IMAGE_MIN_WIDTH = 70;
    public const IMAGE_MAX_SIZE = 5120;
    public const NAME_MIN_LENGTH = 2;
    public const NAME_MAX_LENGTH = 60;

    protected $statusCode = 422;

    protected function prepareForValidation()
    {
        $phone = preg_replace("/[^0-9]/", "",$this->input('phone'));
        $this->merge(['phone' => "+{$phone}"]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                "required",
                sprintf("min:%s", $this::NAME_MIN_LENGTH),
                sprintf("max:%s", $this::NAME_MAX_LENGTH),
            ],
            'email' => 'required|email:rfc',
            'phone' => 'required|regex:/^[\+]{0,1}380([0-9]{9})$/',
            'position_id' => 'required|exists:positions,id',
            'photo' => [
                "required",
                "image",
                "mimes:jpeg,jpg",
                sprintf("max:%s", $this::IMAGE_MAX_SIZE),
                sprintf("dimensions:min_width=%s,min_height=%s", $this::IMAGE_MIN_HEIGHT, $this::IMAGE_MIN_WIDTH),
            ],
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'username',
            'position_id' => 'position',
        ];
    }

}
