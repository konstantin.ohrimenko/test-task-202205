<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\PaginatedResourceResponse as BasePaginatedResourceResponse;


class CustomPaginatedResourceResponse extends BasePaginatedResourceResponse
{
    /**
     * Add the pagination information to the response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function paginationInformation($request)
    {
        $paginated = $this->resource->resource->toArray();

        $wrap = $this->wrapper();
        $total_key = $wrap ? $wrap : 'items';

        return [
            'links' => $this->paginationLinks($paginated),
            "page" => (int)$paginated['current_page'] ?? null,
            "total_pages" => (int)$paginated['last_page'] ?? null,
            "total_{$total_key}" => (int)$paginated['total'] ?? null,
            "count" => (int)$paginated['per_page'] ?? null,
        ];
    }

    /**
     * Get the pagination links for the response.
     *
     * @param  array  $paginated
     * @return array
     */
    protected function paginationLinks($paginated)
    {
        return [
            'prev_url' => $paginated['prev_page_url'] ?? null,
            'next_url' => $paginated['next_page_url'] ?? null,
        ];
    }

}
