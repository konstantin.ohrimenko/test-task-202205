<?php

namespace App\Http\Resources;

class UserCollection extends CustomResourceCollection
{
    public static $wrap = 'users';

    public $collects = UserResource::class;

}
