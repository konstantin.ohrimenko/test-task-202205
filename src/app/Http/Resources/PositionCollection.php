<?php

namespace App\Http\Resources;


class PositionCollection extends CustomResourceCollection
{
    public static $wrap = 'positions';
}
