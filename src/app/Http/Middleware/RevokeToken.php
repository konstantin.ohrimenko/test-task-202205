<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Lcobucci\JWT\Configuration;

class RevokeToken
{
    /**
     * @var Configuration
     */
    private $config;


    /**
     * AuthToken constructor.
     */
    public function __construct(Configuration $config)
    {
        $this->config = $config;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $this->config->parser()->parse($request->header('Token'));
        $seconds = config('jwt.ttl') * 60;
        $jti = $token->claims()->get(\Lcobucci\JWT\Token\RegisteredClaims::ID);
        Cache::tags(['token', 'revoke'])->put($jti, true, $seconds);
        return $next($request);
    }

}
