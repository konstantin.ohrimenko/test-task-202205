<?php

namespace App\Http\Middleware;

use App\Exceptions\Token\TokenExpired;
use Carbon\CarbonImmutable;
use Closure;
use Illuminate\Http\Request;
use Lcobucci\JWT\Configuration;

class ValidateToken
{
    /**
     * @var Configuration
     */
    private $config;


    /**
     * AuthToken constructor.
     */
    public function __construct(Configuration $config)
    {
        $this->config = $config;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     * @throws TokenExpired
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $this->parse($request);
        $constraints = $this->config->validationConstraints();
        if (! $this->config->validator()->validate($token, ...$constraints)) {
            throw new TokenExpired();
        }
        return $next($request);
    }

    /**
     * @throws TokenExpired
     */
    private function parse(Request $request)
    {
        $token = $request->header('Token', '');
        try {
            return $this->config->parser()->parse($token);
        } catch (\Exception $e) {
            throw new TokenExpired();
        }
    }
}
