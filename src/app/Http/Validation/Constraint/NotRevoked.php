<?php

namespace App\Http\Validation\Constraint;


use App\Exceptions\Token\TokenExpired;
use Illuminate\Support\Facades\Cache;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\Validation\Constraint;

class NotRevoked implements Constraint
{

    /**
     * @throws TokenExpired
     */
    public function assert(Token $token): void
    {
        if ($token->claims()->has(\Lcobucci\JWT\Token\RegisteredClaims::ID)) {
            $jti = $token->claims()->get(\Lcobucci\JWT\Token\RegisteredClaims::ID);
            if ($jti && !Cache::tags(['token', 'revoke'])->has($jti)) {
                return;
            }
        }
        throw new TokenExpired();
    }
}
