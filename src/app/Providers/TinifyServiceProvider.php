<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class TinifyServiceProvider extends ServiceProvider
{
    public function boot()
    {
        \Tinify\setKey(config('tinify.key'));
    }

}
