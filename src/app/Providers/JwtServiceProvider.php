<?php

namespace App\Providers;


use App\Http\Validation\Constraint\NotRevoked;
use Carbon\CarbonImmutable;
use Illuminate\Support\ServiceProvider;
use Lcobucci\Clock\FrozenClock;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class JwtServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind(Configuration::class, function() {
            $signer = new Sha256();
            $key = InMemory::plainText(config('jwt.secret'));
            $config = Configuration::forSymmetricSigner(
                $signer,
                $key,
            );
            $clock = new FrozenClock(CarbonImmutable::now());
            $config->setValidationConstraints(
                new \Lcobucci\JWT\Validation\Constraint\IssuedBy(config('app.url')),
                new \Lcobucci\JWT\Validation\Constraint\StrictValidAt($clock),
                new \Lcobucci\JWT\Validation\Constraint\SignedWith($signer, $key),
                new NotRevoked(),
            );
            return $config;
        });


    }
}
