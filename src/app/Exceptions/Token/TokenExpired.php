<?php

namespace App\Exceptions\Token;

use App\Exceptions\CustomValidationException;

class TokenExpired extends CustomValidationException
{
    public function __construct()
    {
        $message = 'The token expired.';
        parent::__construct($message, null, 401);
    }

}
