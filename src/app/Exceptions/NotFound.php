<?php

namespace App\Exceptions;

class NotFound extends CustomValidationException
{
    private $errors = [];

    public function __construct($message = "", $errors = [])
    {
        if (!$message) {
            $message = 'Not found';
        }
        parent::__construct($message, null, 404);

        $this->addError($errors);
    }

    public function addError($errors)
    {
        foreach ($errors as $key => $value) {
            if (!is_array($value)) {
                $value = [$value];
            }
            $this->errors[$key] = $value;
        }
    }

    public function errors()
    {
        return $this->errors;
    }

}
