<?php

namespace App\Exceptions;

class CustomValidationException extends \Exception
{
    private $parent;
    private $statusCode;

    public function __construct($message, $parent = null, $statusCode = 422)
    {
        parent::__construct($message);
        $this->parent = $parent;
        $this->statusCode = $statusCode;
    }

    public function errors()
    {
        if ($this->parent && method_exists($this->parent, 'errors'))
        {
            return $this->parent->errors();
        }
        return [];
    }

    public function statusCode()
    {
        return $this->statusCode;
    }

}
