<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function ($router) {
    Route::get('token', 'App\Http\Controllers\v1\TokenController');
    Route::get('positions', 'App\Http\Controllers\v1\PositionsController');
    Route::get('users', 'App\Http\Controllers\v1\UsersController@index');
    Route::get('users/{user_id}', 'App\Http\Controllers\v1\UsersController@view');
    Route::middleware([
        \App\Http\Middleware\ValidateToken::class,
        \App\Http\Middleware\RevokeToken::class,
    ])
        ->post('users', 'App\Http\Controllers\v1\UsersController@create');
});

