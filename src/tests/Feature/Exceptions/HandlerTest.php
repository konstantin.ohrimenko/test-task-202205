<?php

namespace Tests\Feature\Exceptions;

use App\Exceptions\Handler;
use App\Exceptions\Token\TokenExpired;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tests\TestCase;

class HandlerTest extends TestCase
{

    /**
     * @var Handler
     */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->handler = $this->app->make(Handler::class);
    }

    public function test_throw_token_expired_exception()
    {
        $testTokenExpired = new TokenExpired();
        $request = new Request();
        $jsonResponse = $this->handler->render($request, $testTokenExpired);
        $this->assertInstanceOf(JsonResponse::class, $jsonResponse);
        $response = $jsonResponse->getData(true);
        $this->assertIsArray($response);
        $this->assertArrayHasKey('success', $response);
        $this->assertArrayHasKey('message', $response);
        $this->assertFalse($response['success']);
        $this->assertEquals('The token expired.', $response['message']);
    }
}
