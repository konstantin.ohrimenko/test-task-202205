<?php

namespace Tests\Feature\Http\Middleware;


use App\Http\Middleware\RevokeToken;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Lcobucci\JWT\Configuration;
use Tests\TestCase;

class RevokeTokenTest extends TestCase
{
    /**
     * @var Configuration
     */
    private $config;
    /**
     * @var RevokeToken
     */
    private $middleware;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->app->make(Configuration::class);
        $this->middleware = $this->app->make(RevokeToken::class);
    }

    public function test_token_set_revoked()
    {
        $token = $this->createToken();
        $request = new Request();
        $request->headers->add(['token' => $token->toString()]);
        $seconds = config('jwt.ttl') * 60;
        $jti = $token->claims()->get('jti');
        $tag = \Mockery::mock()
            ->shouldReceive('put')
            ->withArgs([$jti, true, $seconds])
            ->once()
            ->getMock();
        Cache::shouldReceive('tags')
            ->withArgs([['token', 'revoke']])
            ->once()
            ->andReturns($tag);
        $this->middleware->handle($request, function() {});
    }

    private function createToken()
    {
        $time = CarbonImmutable::now()->addSecond(-1);
        return $this->config->builder()
            // Token unique key
            ->identifiedBy(Str::uuid())
            // Configures the issuer (iss claim)
            ->issuedBy(config('app.url'))
            // Configures the time that the token was issue (iat claim)
            ->issuedAt($time)
            ->canOnlyBeUsedAfter($time)
            // Configures the expiration time of the token (exp claim)
            ->expiresAt($time->addMinutes(config('jwt.ttl')))
            ->getToken($this->config->signer(), $this->config->signingKey());
    }

}
