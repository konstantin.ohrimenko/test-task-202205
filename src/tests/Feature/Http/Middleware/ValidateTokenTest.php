<?php

namespace Tests\Feature\Http\Middleware;

use App\Exceptions\Token\TokenExpired;
use App\Http\Middleware\ValidateToken;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Tests\TestCase;

class ValidateTokenTest extends TestCase
{

    /**
     * @var Configuration
     */
    private $config;

    /**
     * @var ValidateToken
     */
    private $middleware;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->app->make(Configuration::class);
        $this->middleware = $this->app->make(ValidateToken::class);
    }

    /**
     * @throws TokenExpired
     */
    public function test_token_is_valid()
    {
        $token = $this->createToken();

        $request = new Request();
        $request->headers->add(['Token' => $token->toString()]);
        $this->middleware->handle($request, function() {});
        $this->assertTrue(true);
    }

    public function test_token_is_expired()
    {
        $now = CarbonImmutable::now()->addMinutes(-(config('jwt.ttl')+1));
        $token = $this->createToken($now);

        $request = new Request();
        $request->headers->add(['Token' => $token->toString()]);
        $this->expectException(TokenExpired::class);
        $this->middleware->handle($request, function() {});
    }

    public function test_token_is_revoked()
    {
        $token = $this->createToken();
        $jti = $token->claims()->get('jti');

        Cache::clearResolvedInstances();
        $tag = \Mockery::Mock()
            ->shouldReceive('has')
            ->withArgs([$jti])
            ->andReturn(true)
            ->getMock();
        Cache::shouldReceive('tags')
            ->withArgs([['token', 'revoke']])
            ->andReturn($tag);

        $request = new Request();
        $request->headers->add(['Token' => $token->toString()]);
        $this->expectException(TokenExpired::class);
        $this->middleware->handle($request, function() {});
        Cache::clearResolvedInstances();
    }

    public function test_wrong_signature()
    {
        $token = $this->createWrongSignatureToken();

        $request = new Request();
        $request->headers->add(['Token' => $token->toString()]);
        $this->expectException(TokenExpired::class);
        $this->middleware->handle($request, function() {});
    }

    public function test_wrong_issuer()
    {
        $token = $this->createWrongIssuerToken();

        $request = new Request();
        $request->headers->add(['Token' => $token->toString()]);
        $this->expectException(TokenExpired::class);
        $this->middleware->handle($request, function() {});
    }

    public function test_wrong_token()
    {
        $request = new Request();
        $this->expectException(TokenExpired::class);
        $this->middleware->handle($request, function() {});
    }

    private function createWrongIssuerToken()
    {
        $builder = $this->generateToken();
        $builder->issuedBy(Hash::make(config('app.url')));
        return $builder->getToken($this->config->signer(), $this->config->signingKey());
    }

    private function createWrongSignatureToken()
    {
        $builder = $this->generateToken();
        return $builder->getToken($this->config->signer(), InMemory::plainText(Hash::make(config('jwt.secret'))));
    }

    private function createToken(CarbonImmutable $time = null)
    {
        $builder = $this->generateToken($time);
        return $builder->getToken($this->config->signer(), $this->config->signingKey());
    }

    private function generateToken(CarbonImmutable $time = null)
    {
        if (is_null($time)) {
            $time = CarbonImmutable::now()->addSecond(-1);
        }
        return $this->config->builder()
            // Token unique key
            ->identifiedBy(Str::uuid())
            // Configures the issuer (iss claim)
            ->issuedBy(config('app.url'))
            // Configures the time that the token was issue (iat claim)
            ->issuedAt($time)
            ->canOnlyBeUsedAfter($time)
            // Configures the expiration time of the token (exp claim)
            ->expiresAt($time->addMinutes(config('jwt.ttl')));
    }

}
