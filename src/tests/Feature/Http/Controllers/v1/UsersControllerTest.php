<?php

namespace Tests\Feature\Http\Controllers\v1;

use App\Http\Requests\UserCreateRequest;
use App\Jobs\TinifyImage;
use App\Models\Position;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class UsersControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_user_successful()
    {
        Queue::fake();
        Storage::fake('public');
        $position = Position::factory(1)->create()->first();

        $tokenResponse = $this->getJson("api/v1/token");
        $json = $tokenResponse->json();
        $token = $json['token'];

        $data = [
            'name' => 'user name',
            'email' => 'demo@exemple.com',
            'phone' => '+380501234567',
            'position_id' => $position->id,
            'photo' => UploadedFile::fake()
                ->image(
                    'avatar.jpg',
                    UserCreateRequest::IMAGE_MIN_WIDTH + 1,
                    UserCreateRequest::IMAGE_MIN_HEIGHT + 1,
                ),
        ];
        $response = $this->withHeaders(['token' => $token])->postJson('api/v1/users', $data);
        $testUser = User::query()->orderByDesc('id')->first();
        $testUserId = $testUser->id;
        // Response
        $response
            ->assertStatus(200)
            ->assertHeader('content-type', 'application/json')
            ->assertJsonFragment([
                'success' => true,
                'user_id' => $testUserId,
                'message' => 'New user successfully registered'
            ]);
        // User attributes
        $attributes = $testUser->only(['id', 'name', 'email', 'phone', 'position_id', 'photo']);
        $testImagePath = "avatar/{$testUserId}.jpg";
        $data['photo'] = $testImagePath;
        $data['id'] = $testUserId;
        $this->assertEquals($data, $attributes);
        // Photo
        Storage::disk('public')->assertExists($testImagePath);
        $testImage = \Image::make(Storage::disk('public')->get($testImagePath));
        $this->assertEquals(70, $testImage->height());
        $this->assertEquals(70, $testImage->width());
        $this->assertEquals(70, $testImage->width());
        // Queue
        Queue::assertPushed(TinifyImage::class);
    }

    public function test_create_user_fail_email_exists()
    {
        $tokenResponse = $this->getJson("api/v1/token");
        $json = $tokenResponse->json();
        $token = $json['token'];

        $position = Position::factory(1)->create()->first();
        $user = User::factory()->create()->first();
        $data = [
            'name' => 'user name',
            'email' => $user->email,
            'phone' => '+380501234567',
            'position_id' => $position->id,
            'photo' => UploadedFile::fake()
                ->image(
                    'avatar.jpg',
                    UserCreateRequest::IMAGE_MIN_WIDTH,
                    UserCreateRequest::IMAGE_MIN_HEIGHT
                ),
        ];
        $response = $this->withHeaders(['token' => $token])->postJson('api/v1/users', $data);
        $response
            ->assertStatus(409)
            ->assertHeader('content-type', 'application/json')
            ->assertJsonFragment([
                'success' => false,
                'message' => 'User with this phone or email already exist'
            ]);
    }

    public function test_create_user_fail_phone_exists()
    {
        $tokenResponse = $this->getJson("api/v1/token");
        $json = $tokenResponse->json();
        $token = $json['token'];

        $position = Position::factory(1)->create()->first();
        $user = User::factory()->create()->first();
        $data = [
            'name' => 'user name',
            'email' => 'demo@example.com',
            'phone' => $user->phone,
            'position_id' => $position->id,
            'photo' => UploadedFile::fake()
                ->image(
                    'avatar.jpg',
                    UserCreateRequest::IMAGE_MIN_WIDTH,
                    UserCreateRequest::IMAGE_MIN_HEIGHT
                ),
        ];
        $response = $this->withHeaders(['token' => $token])->postJson('api/v1/users', $data);
        $response
            ->assertStatus(409)
            ->assertHeader('content-type', 'application/json')
            ->assertJsonFragment([
                'success' => false,
                'message' => 'User with this phone or email already exist'
            ]);
    }

    public function test_request_user_success()
    {
        Position::factory(1)->create();
        $testUser = User::factory(1)->create()->first();

        $response = $this->getJson("api/v1/users/{$testUser->id}");

        $response
            ->assertJsonFragment([
                'success' => true,
                'user' => [
                    'id' => (int)$testUser->id,
                    'name' => $testUser->name,
                    'email' => $testUser->email,
                    'phone' => $testUser->phone,
                    'position' => $testUser->position->name ?? '',
                    'position_id' => (int)$testUser->position_id,
                    'registration_timestamp' => $testUser->created_at->timestamp,
                    'photo' => Storage::url($testUser->photo),
                ]
            ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200);
    }

    public function test_request_user_wrong_user_id()
    {
        $response = $this->getJson("api/v1/users/text");

        $response
            ->assertJsonFragment([
                'success' => false,
                'message' => 'Validation failed',
                "fails" => [
                    "user_id" => ["The user_id must be an integer."]
                ]
            ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(400);
    }

    public function test_request_user_not_found()
    {
        $response = $this->getJson("api/v1/users/1");

        $response
            ->assertJsonFragment([
                'success' => false,
                'message' => 'The user with the requested identifier does not exist',
                "fails" => [
                    "user_id" => ["User not found"]
                ]
            ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(404);
    }

    public function test_request_users_first_page()
    {
        Position::factory(10)->create();
        User::factory(45)->create();
        $response = $this->getJson('api/v1/users?count=10');

        $this->assertResponseStructure($response, 1, 10, 10, 5, 45);

        $links = $response->decodeResponseJson()->json('links');
        $this->assertNull($links['prev_url']);
        $this->assertNotNull($links['next_url']);
    }

    public function test_request_users_second_page()
    {
        Position::factory(10)->create();
        User::factory(45)->create();
        $response = $this->getJson('api/v1/users?count=10&page=2');

        $this->assertResponseStructure($response, 2, 10, 10, 5, 45);

        $links = $response->decodeResponseJson()->json('links');
        $this->assertNotNull($links['prev_url']);
        $this->assertNotNull($links['next_url']);
    }

    public function test_request_users_last_page()
    {
        Position::factory(10)->create();
        User::factory(45)->create();
        $response = $this->getJson('api/v1/users?count=10&page=5');

        $this->assertResponseStructure($response, 5, 10, 5, 5, 45);

        $links = $response->decodeResponseJson()->json('links');
        $this->assertNotNull($links['prev_url']);
        $this->assertNull($links['next_url']);
    }

    private function assertResponseStructure(TestResponse $response, $page, $limit, $count, $total_pages, $total_users)
    {
        $response
            ->assertJsonStructure(
                [
                    'success',
                    'users' => [
                        [
                            'id',
                            'name',
                            'email',
                            'phone',
                            'position',
                            'position_id',
                            'registration_timestamp',
                            'photo',
                        ]
                    ],
                    'links' => [
                        'prev_url',
                        'next_url',
                    ],
                    'page',
                    'total_pages',
                    'total_users',
                    'count',
                ]
            )
            ->assertJsonCount($count, 'users')
            ->assertJsonFragment(
                [
                    'success' => true,
                    'page' => $page,
                    'total_pages' => $total_pages,
                    'total_users' => $total_users,
                    'count' => $limit,
                ]
            )
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200);
    }
}
