<?php

namespace Tests\Feature\Http\Controllers\v1;


use App\Models\Position;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PositionsControllerTest extends TestCase
{

    use RefreshDatabase;

    public function test_request_positions()
    {
        Position::factory(10)->create();
        $response = $this->getJson('api/v1/positions');
        $response->assertJsonStructure(['success', 'positions' => [['id', 'name']]])
            ->assertJsonCount(10, 'positions')
            ->assertJsonFragment(['success' => true])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
        ;

    }
}
