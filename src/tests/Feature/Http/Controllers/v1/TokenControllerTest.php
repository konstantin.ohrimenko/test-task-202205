<?php

namespace Tests\Feature\Http\Controllers\v1;


use Tests\TestCase;

class TokenControllerTest extends TestCase
{

    public function test_request_new_token()
    {
        $response = $this->getJson('api/v1/token');
        $response->assertJsonStructure(['success', 'token'])
            ->assertJsonFragment(['success' => true])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
        ;

    }
}
