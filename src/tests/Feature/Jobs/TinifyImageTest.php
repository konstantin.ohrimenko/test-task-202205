<?php


namespace Tests\Feature\Jobs;


use App\Jobs\TinifyImage;
use App\Models\User;
use Mockery;
use Mockery\MockInterface;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class TinifyImageTest extends TestCase
{
    public function test_job_success()
    {
        Storage::fake('public');
        $testFilePath = 'test.jpg';
        $testImage = UploadedFile::fake()->image($testFilePath, 70, 70);
        Storage::disk('public')->put($testFilePath, $testImage->getContent());
        $testImageSize = Storage::disk('public')->size($testFilePath);

        $this->instance(
            User::class,
            Mockery::mock(User::class, function (MockInterface $mock) use ($testFilePath) {
                $mock->shouldReceive('getAttribute')
                    ->with('photo')
                    ->andReturn($testFilePath);
            })
        );

        $job = $this->app->make(TinifyImage::class);
        $job->handle();
        $resultImageSize = Storage::disk('public')->size($testFilePath);
        $this->assertNotEquals($resultImageSize, $testImageSize);
    }
}
