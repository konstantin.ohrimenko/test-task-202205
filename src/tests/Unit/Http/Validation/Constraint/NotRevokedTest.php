<?php

namespace Tests\Unit\Http\Validation\Constraint;


use App\Exceptions\Token\TokenExpired;
use App\Http\Validation\Constraint\NotRevoked;
use Illuminate\Support\Facades\Cache;
use Lcobucci\JWT\Token;
use PHPUnit\Framework\TestCase;

class NotRevokedTest extends TestCase
{
    /**
     * @var Token\DataSet
     */
    private $claims;

    private $jti = '123456';

    /** @before */
    public function initializeDependencies(): void
    {
        $this->claims = new Token\DataSet(['jti' => $this->jti], 'claims');
    }

    public function testTokenIsValid()
    {
        $token = $this->createToken();
        $this->mockCache();
        $notRevoked = new NotRevoked();
        $notRevoked->assert($token);
        $this->assertTrue(true);
    }

    public function testTokenIsRevoked()
    {
        $token = $this->createToken();
        $this->mockCache(true);
        $this->expectException(TokenExpired::class);
        $notRevoked = new NotRevoked();
        $notRevoked->assert($token);
    }

    private function createToken()
    {
        return \Mockery::Mock(Token::class)
            ->shouldReceive('claims')
            ->andReturn($this->claims)
            ->getMock();
    }

    private function mockCache($has = false)
    {
        $tag = \Mockery::Mock()
            ->shouldReceive('has')
            ->withArgs([$this->jti])
            ->andReturn($has)
            ->getMock();
        Cache::clearResolvedInstances();
        Cache::shouldReceive('tags')
            ->withArgs([['token', 'revoke']])
            ->andReturn($tag)
            ->getMock();
    }
}
