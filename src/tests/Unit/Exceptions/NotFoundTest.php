<?php

namespace Tests\Unit\Exceptions;

use App\Exceptions\NotFound;
use PHPUnit\Framework\TestCase;

class NotFoundTest extends TestCase
{
    /**
     * @return void
     */
    public function test_default_constructor()
    {
        $testException = new NotFound();
        $this->assertEquals('Not found', $testException->getMessage());
        $this->assertEquals([], $testException->errors());
    }

    /**
     * @return void
     */
    public function test_with_params()
    {
        $message = 'message';
        $errors = ['key' => 'value'];
        $testException = new NotFound($message, $errors);
        $this->assertEquals($message, $testException->getMessage());
        $this->assertEquals(['key' => ['value']], $testException->errors());
    }
}
