FROM php:7.4-fpm

USER root

# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libmcrypt-dev \
    libpq-dev \
    libvpx-dev \
    libonig-dev \
    libicu-dev \
    libzip-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libc6 \
    libfreetype6 \
    libgd3 \
    libwebp-dev \
    libxpm-dev \
    libpcre3-dev \
    libfreetype6-dev \
    supervisor \
    zlib1g \
    locales \
    locales-all \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl \
    mc

RUN set -eux; \
	apt-get update; \
	apt-get install -y gosu; \
	rm -rf /var/lib/apt/lists/*; \
# verify that the binary works
	gosu nobody true

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl gd
RUN docker-php-ext-install -j$(nproc) \
    intl
RUN docker-php-ext-configure gd \
    --with-freetype=/usr/include/ \
    --with-webp=/usr/include/ \
    --with-xpm=/usr/include/ \
    --with-jpeg
RUN docker-php-ext-enable gd

RUN pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis

RUN bash -c '[[ -n "$(pecl list | grep xdebug)" ]]\
 || (pecl install xdebug && docker-php-ext-enable xdebug)'

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Cleaning
RUN apt-get clean && apt-get autoremove -y

RUN rm -rf /var/lib/apt/lists/*

# Add user for laravel application
RUN groupadd -f -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www || echo "User already exists"

# Copy existing application directory permissions
COPY --chown=www:www ./src /var/www

# Change current user to www
#USER www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
#CMD ["php-fpm"]
