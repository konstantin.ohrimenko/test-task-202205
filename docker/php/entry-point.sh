#!/bin/bash

touch /var/log/php-fpm.error.log
chown www:www /var/log/php-fpm.error.log

touch /var/log/php-fpm.access.log
chown www:www /var/log/php-fpm.access.log

touch /var/log/cron.artisan.log
chown www:www /var/log/cron.artisan.log

touch /var/log/laravel-worker.log
chown www:www /var/log/laravel-worker.log

echo "starting supervisord"

/usr/bin/supervisord
